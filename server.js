var express = require('express');
var request = require('request');
var app = express();

var auth = {
  u: 'USERNAME',
  h: 'PASSWORD_MD5_HASH'
};

app.get(/^\/d\/(\d+n?)$/, function(req, res) {
  request({
    uri: 'http://osu.ppy.sh/d/' + req.params[0],
    qs: auth,
    followRedirect: false
  }, function (error, response, body) {
    if (!error && (response.statusCode == 302 || response.statusCode == 200)) {
      if (body == 'ERROR: DOWNLOAD_NOT_AVAILABLE') {
        res.sendStatus(404);
      } else {
        res.redirect(response.headers.location.replace(/^https?:\/\//, 'http://' + req.headers.host + '/p/'));
      }
    } else {
      res.sendStatus(500);
    }
  });
});

app.get(/^\/p\/(.*)$/, function(req, res) {
  var headers = {};
  for (var k in req.headers) {
    if (req.headers.hasOwnProperty(k) && k.toLowerCase() !== 'host') {
      headers[k] = req.headers[k];
    }
  }
  var proxy = request({
    uri: 'http://' + req.params[0],
    qs: req.query,
    headers: headers
  });
  proxy.on('response', function(message) {
    res.status(message.statusCode);
    res.set(message.headers);
    message.on('data', function(chunk) {
      res.write(chunk);
    });
    message.on('close', function() {
      res.end();
    });
  });
  proxy.on('end', function() {
    res.end();
  });
  proxy.on('error', function() {
    res.sendStatus(500);
  });
  req.on('close', function() {
    if (proxy && typeof proxy.abort === 'function') {
      proxy.abort();
    }
  });
});

app.listen(9001);
